# Aligned vs Unaligned Memory Access

The C++ code in this repository measures the performance impact of unaligned data accesses. Note that unaligned memory access is undefined behavior in recent C and C++ (C99 or newer, C++11 or newer). Specifically, the benchmark measures CPU time for executing a test problem.

The code requires [Google Benchmark](https://github.com/google/benchmark). It can be built as follows:
```sh
c++ \
	-Wextra -Wall -pedantic -std=c++11 \
	-O3 -march=native -DNDEBUG \
	unaligned-memory-access.cpp \
	$(pkg-config --cflags --libs --static benchmark)
```
To acquire the table below, run the following code, where `compare.py` is the Python script in the `benchmark/tools` directory:
```
python3 compare.py filters \
	./a.out 'BM_aligned_memory<double>' 'BM_unaligned_memory<double>' \
	--benchmark_repetitions=10 \
	| egrep 'mean|median|stddev'
```
To pin the code to one CPU core, prepend `taskset 1`, e.g., `taskset 1 ./a.out`.

On the author's x86-64 CPU, aligned memory accesses are usually slightly faster than unaligned memory accesses (mean, median CPU time). The performance penalty is surprisingly small and depends on multiple factors in addition to the alignment. For example, if the benchmark is pinned to one CPU core with [`taskset`](https://linux.die.net/man/1/taskset), which compiler is used, and on the data type.

| Compiler | taskset? | Type            | mean time(ns,aligned) | mean time(ns,unaligned) | std time(ns,aligned) | std time(ns,unaligned) |
| -- | -- | -- | -- | -- | -- | -- |
| Clang    | 1        | `std::uint64_t` | 362668 | 366575 |  2442 |  1969 |
| GCC      | 1        | `std::uint64_t` | 403481 | 428774 |  2161 |  4855 |
| Clang    | --       | `std::uint64_t` | 374258 | 383623 | 13326 | 14141 |
| GCC      | --       | `std::uint64_t` | 416093 | 439012 |  3050 |  1106 |
| Clang    | 1        | `double`        | 871644 | 890461 |  1968 |  1883 |
| GCC      | 1        | `double`        | 875044 | 891958 |  2284 |  4428 |
| Clang    | --       | `double`        | 872415 | 893587 |  1682 |  4832 |
| GCC      | --       | `double`        | 877680 | 894017 |  1605 |  2638 |
