// Copyright 2020 Inria (https://www.inria.fr/)
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <benchmark/benchmark.h>
#include <cassert>
#include <cstdint>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <numeric>
#include <random>


template<typename T>
static void sum(benchmark::State& state, std::size_t offset) {
	using List = std::vector<T>;

	if(offset >= sizeof(T)) {
		std::fprintf(
			stderr,
			"Expected nonnegative offset < %zu, got %zu\n",
			sizeof(T), offset
		);
		std::exit(EXIT_FAILURE);
	}

	constexpr auto N = std::size_t{1} << 20;

	static_assert(N > 1, "reminder to self");

	auto seed = 1u;
	auto gen = std::mt19937_64(seed);
	auto dist = std::uniform_int_distribution<std::size_t>(N);
	auto xs = List(N);

	for(auto& x: xs) {
		x = dist(gen);
	}

	auto xs_unaligned = std::vector<char>(N * sizeof(T));

	// misalign data
	auto p_destination = xs_unaligned.data() + offset;
	auto num_bytes = (xs.size() - 1) * sizeof(T);

	std::memcpy(p_destination, xs.data(), num_bytes);

	for(auto _ : state) {
		auto p_source_raw = xs_unaligned.data() + offset;
		auto p_source = reinterpret_cast<T*>(p_source_raw);
		auto sum = std::accumulate(p_source, p_source + N - 1, T{0});

		benchmark::DoNotOptimize(sum);
	}
}


template<typename T>
static void BM_aligned_memory(benchmark::State& state) {
	sum<T>(state, 0);
}

template<typename T>
static void BM_unaligned_memory(benchmark::State& state) {
	sum<T>(state, 1);
}


BENCHMARK_TEMPLATE(BM_aligned_memory, double);
BENCHMARK_TEMPLATE(BM_aligned_memory, std::uint64_t);
BENCHMARK_TEMPLATE(BM_aligned_memory, std::uint32_t);
BENCHMARK_TEMPLATE(BM_unaligned_memory, double);
BENCHMARK_TEMPLATE(BM_unaligned_memory, std::uint64_t);
BENCHMARK_TEMPLATE(BM_unaligned_memory, std::uint32_t);

BENCHMARK_MAIN();
