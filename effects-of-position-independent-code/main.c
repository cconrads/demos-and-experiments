// Copyright 2020 Inria (https://www.inria.fr/)
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "library.h"

#include <stdio.h>

int main(int argc, char** argv) {
    (void) argv;
    int x = call_library(argc);

    printf("f(%d) = %d\n", argc, x);
}
