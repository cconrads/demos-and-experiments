# Effects of Position Independent Code

The project demonstrates the effects of position-independent code (PIC) on compiler optizations.

## Introduction

Shared library provide a way to reuse functionality between different programs. A shared library contains a table mapping "names" (symbols) to addresses in the library where functions and variables can be found. When using a shared library, the caller code on disk refers only to symbols; before execution of a program, the caller code as well as the library code are copied into RAM and there, the run-time linker (`ldd` on Linux) replaces all occurrences of symbols in the caller code with the actual addresses of the shared library functionality. Because the location of the library code within RAM is not known at compile-time, the compiler has to generate different code. (Nowadays executables may be located at arbitrary locations in RAM, too, for security reasons. The key term here is _position-independent executable_, or _PIE_ for short).

Users are allowed to "mix" different shared libraries (so-called _symbol interposition_ by the dynamic linker). Consider a shared library called `libshared.so` containing functions `alpha()` and `beta()`, where `alpha()` calls `beta()`:
```c
int beta(int);

int alpha(int x) {
    return beta(x) + 1;
}
```
Users are allowed at run-time to use functions `alpha()` and `beta()` from a different shared librares! For this reason, the compiler must not apply certain optimizations, e.g., inlining. This is a second major difference to non-PIC code.


## Demo

The demo code contains three libraries:
1. a static library,
2. a static library with PIC, and
3. a shared library (with PIC of course).

For each library, there is one executable using or calling the library. The library contains consists of two functions `call_library()` and `identity()`. `call_library()` will be called by the executable and `identity()` will be called by `call_library()`. `identity()` is indeed the identity function, that is `identity(x) == x` (in C syntax). The obvious optimization for the compiler here is to replace all calls to `identity(x)` directly with `x`. We will see below that the compiler is not allowed to perform this optimization with PIC.

Build the code with GCC(!), CMake, and GNU Make:
```sh
mkdir build
cd build
env CC=gcc cmake -DCMAKE_BUILD_TYPE=Release .. # build with optimizations
cmake --build .
```
The build contains three libraries and three executables called `run-shared`, `run-static`, `run-static-fpic` all the output of all executables should be `f(1) = 2`.

We will look at the different assembler codes generated for the function `call_library()`. Run GDB to disassemble the shared library code:
```
$ gdb -batch -ex 'disassemble/rs call_library' libshared.so
Dump of assembler code for function call_library:
   0x00000000000005f0 <+0>:     48 83 ec 08     sub    $0x8,%rsp
   0x00000000000005f4 <+4>:     e8 e7 fe ff ff  callq  0x4e0 <identity@plt>
   0x00000000000005f9 <+9>:     48 83 c4 08     add    $0x8,%rsp
   0x00000000000005fd <+13>:    83 c0 01        add    $0x1,%eax
   0x0000000000000600 <+16>:    c3              retq
End of assembler dump.
```
The instruction at address 0x5f4 contains the call to the function `identity`. Because the user has the freedom to use the function `identity()` from a different library, the compiler cannot optimize away the call to `identity()`. Next, we examine executable using the static library with PIC:
```
$ objdump --disassemble --disassembler-options=intel64 run-static-fpic
[snip]
00000000000008d0 <call_library>:
 8d0:	48 83 ec 08          	sub    $0x8,%rsp
 8d4:	e8 e7 ff ff ff       	callq  8c0 <identity>
 8d9:	48 83 c4 08          	add    $0x8,%rsp
 8dd:	83 c0 01             	add    $0x1,%eax
 8e0:	c3                   	retq
[snip]
```
Observe the call to `identity()` at address 0x8d4. The call was not optimized away but there is no placeholder call either. Finally, we look at the executable without PIC:
```
$ objdump --disassemble --disassembler-options=intel64 run-static
[snip]
00000000000008d0 <call_library>:
 8d0:	8d 47 01             	lea    0x1(%rdi),%eax
 8d3:	c3                   	retq
[snip]
```
The call to `identity()` was inlined. Clearly, PIC stops the compiler from performing certain optimizations.

PIC stops the compiler from performing certain optimizations -- usually. Remove the `build` directory and compile with Clang:
```sh
mkdir build
cd build
env CC=clang cmake -DCMAKE_BUILD_TYPE=Release ..
cmake --build .
```
Here is the static library PIC:
```
0000000000400720 <call_library>:
  400720:	8d 47 01             	lea    0x1(%rdi),%eax
  400723:	c3                   	retq
```
Here is the static library code without PIC:
```

0000000000400720 <call_library>:
  400720:	8d 47 01             	lea    0x1(%rdi),%eax
  400723:	c3                   	retq
```
We see twice the same generated code because Clang because is less strict when it comes to symbol interposition (hat tip to StackOverflow user yugr for this finding, see [_How much overhead can the -fPIC flag add?_](https://stackoverflow.com/a/51611087)). GCC can be instructed to behave (mostly) like Clang with the compiler flag [`-fno-semantic-interposition`](https://gcc.gnu.org/onlinedocs/gcc-8.1.0/gcc/Optimize-Options.html#index-fsemantic-interposition). As of August 2019, Clang _cannot_ generate interposition-compliant code (cf. [Phabricator D65616](https://reviews.llvm.org/D65616)).


## Re-enabling Optimization of PIC

Reconsider the library above with its two functions `call_library()` and `identity()`. Imagine now that `identity()` is an internal function or an implementation detail which should be ignored by users. By default, the function `identity` will be exposed to symbol interposition because it can be found in the symbol table of the shared library:
```
$ nm --dynamic --extern-only --defined-only libshared.so
00000000000005f0 T call_library
00000000000005e0 T identity
[snip]
```
If we give `identity()` hidden visibility now, then `identity` will vanish from the symbol table and the compiler can optimize the PIC (note that the default visibility of `call_library` is specified in the file `library.h`):
```
$ env CC=gcc CFLAGS='-fvisibility=hidden' cmake -DCMAKE_BUILD_TYPE=Release ..
[snip]
$ cmake --build .
[snip]
$ nm --dynamic --extern-only --defined-only libshared.so
00000000000005a0 T call_library
[snip]
$ gdb -batch -ex 'disassemble/rs call_library' libshared.so
Dump of assembler code for function call_library:
   0x00000000000005a0 <+0>:	8d 47 01	lea    0x1(%rdi),%eax
   0x00000000000005a3 <+3>:	c3	retq
End of assembler dump.
```
Observe the compiler can now inline the call to `identity()` even in the shared library.


## Further Reading

PIC code is not obligatory when building a shared library. Instead, load-time relocation can be used, see Eli Benderski's article and look up the compiler option `-mimpure-text`. Ulrich Drepper explains how shared libraries work, how their ABI can be maintained, and how they can be optimized (this includes modifications to the default visibility as exploited above).

* Eli Benderski: ["Load-time relocation of shared libraries"](https://eli.thegreenplace.net/2011/08/25/load-time-relocation-of-shared-libraries/) (2011).
* Eli Benderski: ["Position Independent Code (PIC) in shared libraries"](https://eli.thegreenplace.net/2011/11/03/position-independent-code-pic-in-shared-libraries/) (2011).
* Ulrich Drepper: ["How To Write Shared Libraries"](https://www.akkadia.org/drepper/dsohowto.pdf) (2011).
