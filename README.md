# Demos and Experiments

This repository contains a variety of simple demos and experiments related to C, C++, and CMake. Topics include dependency management with CMake and system-level issues like dynamic linking or undefined behavior.
