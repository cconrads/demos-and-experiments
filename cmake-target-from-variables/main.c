// Copyright 2020 Inria (https://www.inria.fr/)
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <math.h>
#include <stdio.h>

int main() {
    double x = M_PI_2;
    double s = sin(x);

    printf("sin(%f) = %f\n", x, s);
}
