# CMake: Targets from Variables

The CMake build in this directory demonstrates how to use imported targets to set up a build when a dependency can either be found on the user's system or be built by CMake.

Pass `-DUSE_SYSTEM_LIBM=ON` to use the system libm and `-DUSE_SYSTEM_LIBM=OFF` to use the simple libm flavor shipping with this software.
