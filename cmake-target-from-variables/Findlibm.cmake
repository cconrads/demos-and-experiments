# Copyright 2020 Inria (https://www.inria.fr/)
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

find_path(Math_INCLUDE_DIR math.h)
find_library(Math_LIBRARY m)

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(libm Math_INCLUDE_DIR Math_LIBRARY)
