// Copyright 2020 Inria (https://www.inria.fr/)
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include "common.h"

int internal_attr(int i) __attribute__((visibility("hidden")));
int internal_attr(int i) {
    return i;
}

static int internal_static(int i) {
    (void) i;
    return 0;
}

int common(int i) {
	return -internal_attr(i) + internal_static(i);
}
