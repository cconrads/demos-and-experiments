// Copyright 2020 Inria (https://www.inria.fr/)
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <stdio.h>
int alpha(int);
int beta(int);
int common(int);


int main() {
    int a = alpha(10);
    int b = beta(10);
    int c = common(10);

    printf("a=%d b=%d c=%d\n", a, b, c);
}
