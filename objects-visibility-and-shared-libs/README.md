# Object Files, Visibility, and Shared Libraries

CMake can be instructed to reuse object files for multiple targets. This technique does not work out of the box if one of the targets is a shared library because then the object file must contain position independent code. The CMake setup in this directory builds multiple libraries depending on object files. However because of the visibility of the functions in the object files, the object files never need to contain position independent code.
